<?php

namespace extendedmessaging;

use Yii;
use yii\base\Event;

class Events
{
    public static function onMessageInit(Event $event)
    {
        if (Yii::$app->getModule('extended-messaging')->canView()) {
            $jsCode = " /* JavaScript code to move #icon-messages content */ ";
            Yii::$app->view->registerJs($jsCode, \yii\web\View::POS_END);
        }
    }
}
