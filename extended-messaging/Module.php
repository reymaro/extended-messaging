<?php

namespace extendedmessaging;

use Yii;
use humhub\components\Module as BaseModule;

class Module extends BaseModule
{
    public function init()
    {
        parent::init();
    }

    public function getConfigUrl()
    {
        return Yii::$app->urlManager->createUrl(['/extended-messaging/config']);
    }

    public function canView()
    {
        $isDevMode = $this->settings->get('developmentMode', false);
        return !$isDevMode || Yii::$app->user->isAdmin();
    }
}
