<?php

namespace extendedmessaging\assets;

use yii\web\AssetBundle;

class ExtendedMessagingAsset extends AssetBundle
{
    public $sourcePath = '@extendedmessaging/resources';
    public $css = [
        'css/extended-messaging.css',
    ];
    public $js = [
        'js/extended-messaging.js',
    ];
}
