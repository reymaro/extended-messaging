<?php

return [
    'id' => 'extended-messaging',
    'class' => 'extendedmessaging\Module',
    'namespace' => 'extendedmessaging',
    'events' => [
        ['class' => humhub\modules\content\widgets\WallEntryLinks::class, 'event' => humhub\modules\content\widgets\WallEntryLinks::EVENT_INIT, 'callback' => ['extendedmessaging\Events', 'onMessageInit']],
    ],
];
