<?php

namespace extendedmessaging\controllers;

use Yii;
use humhub\components\Controller;
use extendedmessaging\models\ConfigureForm;

class ConfigController extends Controller
{
    public function actionIndex()
    {
        $form = new ConfigureForm();
        if ($form->load(Yii::$app->request->post()) && $form->validate()) {
            $form->save();
            Yii::$app->session->setFlash('data-saved', Yii::t('ExtendedMessagingModule.base', 'Saved'));
            return $this->redirect(['index']);
        }
        
        return $this->render('index', ['model' => $form]);
    }
}
