<?php

namespace extendedmessaging\models;

use Yii;
use yii\base\Model;

class ConfigureForm extends Model
{
    public $developmentMode;

    public function rules()
    {
        return [
            ['developmentMode', 'boolean'],
        ];
    }

    public function init()
    {
        parent::init();
        $this->developmentMode = Yii::$app->getModule('extended-messaging')->settings->get('developmentMode');
    }

    public function save()
    {
        Yii::$app->getModule('extended-messaging')->settings->set('developmentMode', $this->developmentMode);
        return true;
    }
}
