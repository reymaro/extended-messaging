<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>

<div class="panel panel-default">
    <div class="panel-heading"><?= Yii::t('ExtendedMessagingModule.base', '<strong>Extended Messaging</strong> settings'); ?></div>
    <div class="panel-body">

        <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'developmentMode')->checkbox(); ?>

        <div class="form-group">
            <?= Html::submitButton(Yii::t('ExtendedMessagingModule.base', 'Save'), ['class' => 'btn btn-primary']); ?>
        </div>

        <?php ActiveForm::end(); ?>

        <?php if (Yii::$app->session->hasFlash('data-saved')): ?>
            <div class="alert alert-success">
                <?= Yii::$app->session->getFlash('data-saved'); ?>
            </div>
        <?php endif; ?>

    </div>
</div>
