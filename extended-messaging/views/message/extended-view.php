<?php

use yii\helpers\Html;

?>

<div class="panel panel-default">
    <div class="panel-heading"><?= Yii::t('ExtendedMessagingModule.base', 'Extended Message View'); ?></div>
    <div class="panel-body">
        <!-- Contenido de la vista extendida -->
        <?= Html::encode($model->content); ?>
    </div>
</div>
<div id="extended-sidebar"></div>
