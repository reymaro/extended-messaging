<?php

use yii\helpers\Html;

?>

<div class="panel panel-default">
    <div class="panel-heading"><?= Yii::t('ExtendedMessagingModule.base', 'Extended Messaging Button'); ?></div>
    <div class="panel-body">
        <!-- Botón personalizado -->
        <?= Html::a('Extended Messaging', ['/extended-messaging/message/extended'], ['class' => 'btn btn-primary']); ?>
    </div>
</div>
