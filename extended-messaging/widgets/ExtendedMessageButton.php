<?php

namespace extendedmessaging\widgets;

use humhub\widgets\Widget;
use Yii;

class ExtendedMessageButton extends Widget
{
    public function run()
    {
        return $this->render('extendedMessageButton');
    }
}
